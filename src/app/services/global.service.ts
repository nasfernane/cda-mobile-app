import { MapType } from '@angular/compiler';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class GlobalService {
  constructor(
  ) { }

  static capitalize(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  // fonction pour remplaçer les try/catch blocs dans les fonctions async.
  static catchAsync(f: any) {
    return async function() {
        try {
            return await f.apply(this, arguments);
        } catch(e) {
            console.error(e);
        }
    };
  }

  static storeUserData(args: Map<string, string>) {
    for (const [key, value] of args) {
      localStorage.setItem(key, value);
    }
  }

  static getAccessToken() {
    return localStorage.getItem('accessToken');
  }

  static getXsrfToken() {
    return localStorage.getItem('xsrfToken');
  }

  // async handleRequest(instance: AxiosInstance, method: string, path: string) {
  //   try {
  //     const res = await this.dispatchRequest(instance, method, path);

  //     const status = String(res.status);

  //     if (status.startsWith('4') || status.startsWith('5')) {
  //       const tokenRefreshed = await this.authService.refreshToken();

  //       if (tokenRefreshed) {
  //         await this.dispatchRequest(instance, method, path);
  //       }
  //     } else {
  //       return res;
  //     }

  //   } catch(err) {
  //     console.error(err);
  //   }
  // }

  // async dispatchRequest(instance: AxiosInstance, method: string, path: string) {
  //   try {
  //     let res: any;
  //     switch (method) {
  //       case 'get':
  //         res = await instance.get(path);
  //         break;
  //       case 'post':
  //         res = await instance.post(path);
  //         break;
  //       case 'put':
  //         res = await instance.put(path);
  //         break;
  //       case 'delete':
  //         res = await instance.delete(path);
  //         break;
  //     }

  //     return res;
  //   } catch(err) {
  //     console.error(err);
  //   }
  // }
}
