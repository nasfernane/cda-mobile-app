import { Injectable } from '@angular/core';
import axios from 'axios';
import { GlobalService } from './global.service';
import { RefreshInterceptor } from '../interceptors/refresh.interceptor';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class UsersService {
  router = Router;
  refreshInterceptor = new RefreshInterceptor();

  axiosInstance = axios.create({
    baseURL: 'https://api.pote.dev',
    headers: {
        Authorization: 'Bearer ' + GlobalService.getAccessToken(),
        'x-xsrf-token': GlobalService.getXsrfToken(),
    },
    params: {
      withCredentials: true
    }
  });

  constructor(
  ) { }

  async getAllUsers() {
    axios.interceptors.request.use(await this.refreshInterceptor.refreshToken(this.axiosInstance));
    const res = await this.axiosInstance.get('/users/public');
    return res.data;
  }

  async getUserFromEmail(email) {
    axios.interceptors.request.use(await this.refreshInterceptor.refreshToken(this.axiosInstance));
    const res = await this.axiosInstance.get(`users/email/${email}`);
    return res.data;
  }

}
