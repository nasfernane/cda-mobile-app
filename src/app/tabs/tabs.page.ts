import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationService } from '../services/authentification.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  constructor(
    private authService: AuthentificationService,
    private router: Router,
  ) {}

  async ngOnInit() {
    // si utilisateur non connecté, on redirige
    if (!this.authService.isLogged()) {
      this.router.navigate(['/login']);
    }
  }
}
