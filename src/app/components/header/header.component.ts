import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})

export class HeaderComponent implements OnInit {
  userConnected: boolean;
  userName: string;

  constructor(
    private authService: AuthentificationService,
    private usersService: UsersService,
  ) { }


  ngOnInit() {
    this.userConnected = this.authService.isLogged();
    this.userName = localStorage.getItem('userName');
  }

  logout() {
    this.authService.logout();
  }
}
