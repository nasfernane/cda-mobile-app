import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.page.html',
  styleUrls: ['./password-reset.page.scss'],
})
export class PasswordResetPage implements OnInit {
  resetPwForm: FormGroup;
  validToken: boolean;
  token: string;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthentificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.validToken = true;
    this.resetPwForm = this.formBuilder.group({
      token: ['', [Validators.required, Validators.minLength(2)]],
      password: ['', [Validators.required, Validators.minLength(2)]],
      passwordConfirm: ['', [Validators.required, Validators.minLength(2)]],
    });

    this.token = this.activatedRoute.snapshot.paramMap.get('id');

    if (this.token) {
      this.resetPwForm.controls.token.setValue(this.token);
    }
  }

  async resetPw() {
    const token = this.resetPwForm.value.token;
    const password = this.resetPwForm.value.password;
    const passwordConfirm = this.resetPwForm.value.passwordConfirm;

    try {
      const pwIsReset = await this.authService.resetPw(token, password, passwordConfirm);
      this.validToken = true;
      this.router.navigate(['/login']);
    } catch(err) {
      this.validToken = false;
      console.error(err);
    }
  }
}
