import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  validLogin: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthentificationService,
    private router: Router,
    ) { }

  ngOnInit() {
    this.validLogin = true;
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      password: ['', [Validators.required, Validators.minLength(2)]],
    });
  }

  async login() {
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;

    try {
      await this.authService.login(email, password);
      this.validLogin = false;
      this.router.navigate(['/tabs']);
    } catch(err) {
      this.validLogin = false;
      console.error(err);
    }
  }
}
