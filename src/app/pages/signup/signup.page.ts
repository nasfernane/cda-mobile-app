import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  signupForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public authService: AuthentificationService,
    private router: Router,
    ) {}

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(2)]],
      userName: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      password: ['', [Validators.required, Validators.minLength(2)]],
      passwordConfirm: ['', [Validators.required, Validators.minLength(2)]],
    });
  }

  register() {
    const firstName = this.signupForm.value.firstName;
    const lastName = this.signupForm.value.lastName;
    const userName = this.signupForm.value.userName;
    const email = this.signupForm.value.email;
    const password = this.signupForm.value.password;
    const passwordConfirm = this.signupForm.value.passwordConfirm;

    const accountCreated = this.authService.register(firstName, lastName, userName, email, password, passwordConfirm);

    if (accountCreated) {
      this.router.navigate(['/tabs']);
    }
  }

}
