import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-password-lost',
  templateUrl: './password-lost.page.html',
  styleUrls: ['./password-lost.page.scss'],
})
export class PasswordLostPage implements OnInit {
  lostPwForm: FormGroup;
  pwResetSuccess: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthentificationService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.pwResetSuccess = true;
    this.lostPwForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
    });
  }

  async lostPw() {
    const email = this.lostPwForm.value.email;

    try {
      const pwIsReset = await this.authService.lostPw(email);
      this.pwResetSuccess = true;
      this.router.navigate(['/password-reset']);
    } catch(err) {
      this.pwResetSuccess = false;
      console.error(err);
    }
  }
}
