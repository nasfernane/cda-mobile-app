import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { IonicModule } from '@ionic/angular';
import { PasswordLostPageRoutingModule } from './password-lost-routing.module';
import { PasswordLostPage } from './password-lost.page';
import { HeaderComponentModule } from 'src/app/components/header/header.module';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    PasswordLostPageRoutingModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    HeaderComponentModule,
    MatCardModule
  ],
  declarations: [PasswordLostPage]
})
export class PasswordLostPageModule {}
