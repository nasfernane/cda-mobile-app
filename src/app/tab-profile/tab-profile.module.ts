import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabProfilePage } from './tab-profile.page';
import { HeaderComponentModule } from '../components/header/header.module';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';

import { TabProfilePageRoutingModule } from './tab-profile-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    HeaderComponentModule, 
    TabProfilePageRoutingModule,
    MatCardModule,
    MatListModule
  ],
  declarations: [TabProfilePage]
})
export class Tab2PageModule {}
