import { Component, OnInit } from '@angular/core'
import { UsersService } from '../services/users.service';;
import { GlobalService } from '../services/global.service';
import { AuthentificationService } from '../services/authentification.service';

@Component({
  selector: 'app-profile',
  templateUrl: 'tab-profile.page.html',
  styleUrls: ['tab-profile.page.scss']
})
export class TabProfilePage implements OnInit {
  user: any;

  constructor(
    private authService: AuthentificationService,
  ) {}

  async ngOnInit() {
    this.user = await this.authService.getCurrentUser();
  }
}
