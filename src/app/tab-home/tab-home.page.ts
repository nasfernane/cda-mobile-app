import { formattedError } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service';


import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-tab-home',
  templateUrl: 'tab-home.page.html',
  styleUrls: ['tab-home.page.scss']
})

export class TabHomePage implements OnInit {
  users: any;

  constructor(
    private usersService: UsersService,
  ) {}

  async ngOnInit() {
    const users = await this.usersService.getAllUsers();
    this.users = users;

    this.users.forEach(user => {
      user.lastname = GlobalService.capitalize(user.lastname);
      user.firstname = GlobalService.capitalize(user.firstname);
    });
  }

}
