import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { TabHomePage } from './tab-home.page';
import { HeaderComponentModule } from '../components/header/header.module';

import { TabHomePageRoutingModule } from './tab-home-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    HeaderComponentModule,
    TabHomePageRoutingModule,
    MatListModule,
    MatDividerModule
  ],
  declarations: [TabHomePage]
})
export class Tab1PageModule {}
