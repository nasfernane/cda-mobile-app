import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabSearchPage } from './tab-search.page';
import { HeaderComponentModule } from '../components/header/header.module';
import { TabSearchPageRoutingModule } from './tab-search-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { UserCardComponentModule } from '../components/user-card/user-card.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HeaderComponentModule,
    RouterModule.forChild([{ path: '', component: TabSearchPage }]),
    TabSearchPageRoutingModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatListModule,
    UserCardComponentModule
  ],
  declarations: [TabSearchPage]
})
export class Tab3PageModule {}
