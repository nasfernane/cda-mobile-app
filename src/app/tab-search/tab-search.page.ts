import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-tab-search',
  templateUrl: 'tab-search.page.html',
  styleUrls: ['tab-search.page.scss']
})
export class TabSearchPage implements OnInit {
  userSearchForm: FormGroup;
  user: any;
  validSearch = true;

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
  ) {}

  async ngOnInit() {
    this.userSearchForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
    });
  }

  async searchUser(email) {
    try {
      this.user = await this.usersService.getUserFromEmail(this.userSearchForm.controls.email.value);
      this.validSearch = true;
      this.user.lastname = GlobalService.capitalize(this.user.lastname);
      this.user.firstname = GlobalService.capitalize(this.user.firstname);

    } catch(err) {
      console.error(err);
      this.validSearch = false;
      this.user = {};
    }

  }

}
